﻿using UnityEngine;
using System.Collections;

public class menu : MonoBehaviour {

	GameObject exit;

	payment payment;

	public int upgradeId;

	// Use this for initialization
	void Start () {

		payment = GetComponent<payment> ();

		if(!PlayerPrefs.HasKey("upgradeId")){

			upgradeId = 1;
			PlayerPrefs.SetInt ("upgradeId", 1);
		}

		else{

			upgradeId = PlayerPrefs.GetInt("upgradeId");


		}

		exit = GameObject.Find ("Canvas/Button");

		#if UNITY_WEBGL

		Destroy(exit);

		#endif
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void ExitGame(){

		Application.Quit();

	}

	public void PayUpgrade(){
	
		int pay = payment.Pay (upgradeId);
	
		switch (pay) {

		case 0:
			Debug.Log ("Da ist bei der ZAhlung wohl was schief gegangen...");
			break;
		default:
			Debug.Log("Katsching!");
			upgradeId++;
			PlayerPrefs.SetInt("upgradeId", upgradeId);
			PlayerPrefs.Save();
			break;
		}
	}

}
